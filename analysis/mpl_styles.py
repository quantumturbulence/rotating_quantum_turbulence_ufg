"""Different matplotlib style options for customizing plots.

To allow for more flexibility, we provide these as dictionaries.  (This allows one to
use hashes for example, which are not valid in an mplstyle file.)

Use as you would styles:

>>> import matplotlib.pyplot as plt
>>> import mpl_styles
>>> plt.style.use(mpl_styles.cycles)

Or, better, use it locally:

>>> with plt.style.context(mpl_styles.cycles, after_reset=True):
...     plt.plot([1,2], [1,2])

etc.
"""
import contextlib, importlib
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt

# https://miguendes.me/the-best-way-to-compare-two-dictionaries-in-python
from deepdiff import DeepDiff


__all__ = ["tufte_style", "prl_style"]


def diff_styles(style1, style2):
    rc1 = {}
    rc2 = {}

    with plt.style.context(style1, after_reset=True):
        rc1.update(plt.rcParams)

    with plt.style.context(style2, after_reset=True):
        rc2.update(plt.rcParams)

    return DeepDiff(rc1, rc2)


def get_constants(**kw):
    """Return a  constants dict calculating everything in terms of points."""
    constants = dict(pt_per_inch=72.27, golden_mean=2 / (1 + np.sqrt(5)),)

    constants.update(kw)

    for key_pt in [_k for _k in constants if _k.endswith("_pt")]:
        key = key_pt[:-3]
        constants[key] = constants[key_pt] / constants["pt_per_inch"]
    return constants


######################################################################
# For plots with the Tufte style

tufte_dict = {
    "text.usetex": True,
    "text.latex.preamble": r"\usepackage{mathpazo}",
    "font.family": "serif",
    # "font.serif": "Palatino",
    "axes.titlesize": 10.0,
    "axes.labelsize": 10.0,
    "xtick.labelsize": 8.0,
    "ytick.labelsize": 8.0,
}


@contextlib.contextmanager
def tufte_style(
    figuretype="figure",
    height="golden_mean*width",
    base="seaborn-darkgrid",
    after_reset=True,
    rc_dict=None,
):
    style = dict(tufte_dict)
    if rc_dict:
        style.update(rc_dict)
    constants = get_constants(
        fullwidth_pt=480.0,  # \begin{figure*}\showthe\textwidth\end{figure*}
        textwidth_pt=312.0,  # \begin{figure}\showthe\textwidth\end{figure}
        marginwidth_pt=144.0,  # \begin{marginfigure}\showthe\textwidth\end{marginfigure}
    )
    width = {
        "figure": "textwidth",
        "figure*": "fullwidth",
        "marginfigure": "marginwidth",
    }[figuretype]

    constants["width"] = eval(str(width), constants)
    constants["height"] = eval(str(height), constants)
    style["figure.figsize"] = (constants["width"], constants["height"])

    with plt.style.context([base, style], after_reset=after_reset):
        yield


######################################################################
# For plots with PRL style

prl_dict = {
    "text.usetex": True,
    "text.latex.preamble": "".join(
        [
            r"\usepackage{newtxtext,newtxmath}",
            r"\usepackage{amsmath}",
            r"\providecommand{\mathdefault}[1][]{}",
        ]
    ),
    "font.family": "serif",
    # "font.serif": "Times New Roman",
    "axes.titlesize": 10.0,
    "axes.labelsize": 10.0,
    "xtick.labelsize": 8.0,
    "ytick.labelsize": 8.0,
    # Axes properties, spines, grids etc.
    # "axes.linewidth": 1.0,
    "axes.edgecolor": "gray",
    "axes.grid": True,
    "grid.color": "WhiteSmoke",
    "grid.alpha": 0.3,
    "figure.dpi": 300,
}


@contextlib.contextmanager
def prl_style(
    figuretype="figure",
    height="golden_mean*width",
    base="seaborn-paper",
    after_reset=True,
    rc_dict=None,
):
    style = dict(prl_dict)
    if rc_dict:
        style.update(rc_dict)
    constants = get_constants(
        columnwidth_pt=246.0,  # \begin{figure}\showthe\columnwidth\end{figure}
        fullwidth_pt=510.0,  # \begin{figure*}\showthe\textwidth\end{figure*}
    )
    width = {"figure": "columnwidth", "figure*": "fullwidth",}[figuretype]

    constants["width"] = eval(str(width), constants)
    constants["height"] = eval(str(height), constants)
    style["figure.figsize"] = (constants["width"], constants["height"])

    with plt.style.context([base, style], after_reset=after_reset):
        yield
