"""Various classes to load and explore the data."""

from collections import namedtuple
from pathlib import Path

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec

import mmf_setup.set_path

_ROOT = Path(mmf_setup.set_path())

_DATA_DIR = _ROOT / "data"

__all__ = ["VortexData"]


class VortexData(object):
    """
    Attributes
    ----------
    end_skip : int, None
       Number of points at the end of the run to skip.  (These tend to be noisy.)
    data_dir : Path
       Location of data files.

    The datafile contains:

    tx, ty, tz : float
       Average of unit tangent vector component over all vortex elements.

    """

    VortexLineData = namedtuple(
        "VortexLineData",
        [
            "t_",
            "Nv",
            "L_",
            "Linf_",
            "tx",
            "ty",
            "tz",
            "bound",
            "touch",
            "winding_number_histogram",
            "boundary_touches",
            "tb_",
            "sigma_L_",
        ],
    )

    _files = {
        "SLDAP0": ("qt0dyn_data.txt", "qt0dyn_nbtouches.eps=2.00.txt"),
        "SLDAP10": ("qt1dyn_data.txt", "qt1dyn_nbtouches.eps=2.00.txt"),
        "SLDAP20": ("qt2dyn_data.txt", "qt2dyn_nbtouches.eps=2.00.txt"),
        "ETF": ("qtetf4solr_data.txt", "qtetf4solr_nbtouches.eps=2.00.txt"),
    }

    # Number of vortices in ground state.
    _N_infs = {
        "SLDAP0": 12,  # Checked
        "SLDAP10": 12,  # Checked
        "SLDAP20": 12,  # Not checked, but not used because it is very noisy
        "ETF": 12,  # Checked
    }

    # Time at which knives are turned off.  We define this as t = 0
    _t0_ = {
        "SLDAP0": 260,  # Checked
        "SLDAP10": 220,  # Checked
        "SLDAP20": 220,  # Checked
        "ETF": 260,  # Checked
    }

    # Manually determined times (after t0_) at which the boundary touches do not change.
    _tb_ = {
        "SLDAP0": 200,
        "SLDAP10": 252,
        "SLDAP20": 275,
        "ETF": 210,
    }

    kF = 1.0
    hbar = 1.0
    m = 1.0
    eF = (hbar * kF) ** 2 / 2 / m
    t_unit = hbar / eF
    L_unit = 1 / kF
    Lz_ = 100  # Length of box

    def __init__(self, end_skip=None, data_dir=Path("../data/")):
        self.data_dir = data_dir
        self.end_skip = end_skip

    def keys(self):
        return list(self._files.keys())

    def _get_sigma_L_(self, t_, L_, window_length=101, polyorder=3):
        """Compute the standard deviations required to make chi^2 = 1
        when fitting a polynomial of order `polyorder` to data windows of the specified
        length.
        """
        i_max = np.argmax(L_)
        ts_, Ls_ = t_[i_max:], L_[i_max:]
        is_ = np.arange(0, len(ts_) - window_length)

        ts = []
        sigmas = []

        def get_chi(i):
            inds = slice(i, i + window_length)
            P = np.polyfit(ts_[inds], Ls_[inds], deg=polyorder)
            return np.sqrt(((np.polyval(P, ts_[inds]) - Ls_[inds]) ** 2).mean())

        for i in is_:
            ts.append(ts_[i:][:window_length].mean())
            sigmas.append(get_chi(i))
        return np.interp(t_, ts, sigmas)

    def load(self, key, sigma_window_length=101, sigma_polyorder=3):
        """Return VortexLineData for the specified key.

        This namedtuple has fields like `t_ = t/t_unit` with an underscore signifying
        that they have been made dimensionless.
        """
        # file1 = length, file2 = boundary touches
        file1, file2 = [self.data_dir / _file for _file in self._files[key]]
        data1 = np.loadtxt(file1)[: self.end_skip].T
        t_, Nv, L_, tx, ty, tz, bound, touch = data1[:8]
        winding_number_histogram = data1[8:]

        # Loading data for number of boundary touches
        # KK: I made here some changes:
        #     - I used only first column in old figure (there are differnt methods for
        #       computing this qunatity, I checked that the first is most reliable)
        #     - I performed some smoothing with moving averange
        data2 = np.loadtxt(file2)[: self.end_skip].T
        t2_ = data2[0]
        boundary_touches = data2[1]
        for i in range(boundary_touches.shape[0] - 1):
            boundary_touches[i + 1] = 0.5 * (
                boundary_touches[i + 1] + boundary_touches[i]
            )
        assert np.allclose(t_, t2_)

        # Number of vortices in final state.
        N_inf = self._N_infs[key]

        # Offset so t=0 is where the knives are turned off.
        t0_ = self._t0_[key]
        t_ = t_ - t0_

        i_max = np.argmax(L_)
        res = self.VortexLineData(
            t_=t_,
            Nv=Nv,
            L_=L_,
            Linf_=self.Lz_ * N_inf,
            tx=tx,
            ty=ty,
            tz=tz,
            bound=bound,
            touch=touch,
            winding_number_histogram=winding_number_histogram,
            boundary_touches=boundary_touches,
            tb_=self._tb_[key],
            sigma_L_=self._get_sigma_L_(
                t_=t_,
                L_=L_,
                window_length=sigma_window_length,
                polyorder=sigma_polyorder,
            ),
        )
        return res

    def plot(self, keys=None, fig=None, figsize=None, figlen=(5, 3)):
        """Demo plots of data.

        Arguments
        ---------
        keys : [str], None
           List of keys to plot.
        fig : Figure, None
           Figure to use for plotting.
        figsize : (float,float), None
           Size of figure (if fig=None).
        figlen : (3, 2)
           Size of each sub-figure: used to compute figsize if not provided.
        """
        if keys is None:
            keys = list(self._files)

        Ncol = 4
        if fig is None:
            if figsize is None:
                figsize = (figlen[0] * Ncol, figlen[1] * len(keys))
            fig = plt.figure(figsize=figsize)
        master_grid = GridSpec(len(keys), 1)

        for _n, (key, grid) in enumerate(zip(keys, master_grid)):
            title = None
            res = self.load(key)
            gs = GridSpecFromSubplotSpec(1, Ncol, subplot_spec=grid)
            ax = fig.add_subplot(gs[0])

            ax.plot(res.t_, res.L_, label=key)
            ylim = ax.get_ylim()
            ax.plot(res.t_, res.Nv * self.Lz_, label="Nv*Lz total")
            if _n == 0:
                title = "Length"
            ax.set(xlabel="$t e_F$", ylabel="$L k_F$", ylim=ylim, title=title)
            ax.legend()

            ax = fig.add_subplot(gs[1])
            ax.plot(res.t_, res.tx, label="tx")
            ax.plot(res.t_, res.ty, label="ty")
            ax.plot(res.t_, res.tz, label="tz")
            if _n == 0:
                title = "tx, ty, tz"
            ax.set(xlabel="$t e_F$", ylabel="$t$")
            ax.legend()

            ax = fig.add_subplot(gs[2])
            if _n == 0:
                title = "Vortex counts"
            ax.plot(res.t_, res.Nv, label="Nv total")
            ax.plot(res.t_, res.boundary_touches.T, label="#_b")
            # ax.plot(res.t_, res.bound, label="bound")
            # ax.plot(res.t_, res.touch, label="touch")
            ax.set(xlabel="$t e_F$", ylabel="$N$", title=title)
            ax.legend()

            ax = fig.add_subplot(gs[3])
            ax.plot(res.t_, res.winding_number_histogram.T)
            if _n == 0:
                title = "Winding number histogram?"
            ax.set(xlabel="$t e_F$", ylabel="number?", title=title)
            ax.legend()
