25 May 2021
===========
* From the Savoy-Golay filtering of the derivatives, I think that window_length N=101 or
  N=151 should be used.  The former keeps some of the main features, while the latter
  smooths these, giving the overall trend.
* 
