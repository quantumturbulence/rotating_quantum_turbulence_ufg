Rotating Quantum Turbulence in the Unitary Fermi Gas
====================================================
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/quantumturbulence%2Frotating_quantum_turbulence_ufg/branch/default?filepath=Figures%2FVortexLength.ipynb)

This repository contains data and analysis from our paper "Rotating Quantum Turbulence
in the Unitary Fermi Gas":

* [arXiv:2010.07464](https://arxiv.org/abs/2010.07464)

It is self-contained including the data used to generate Figure 3. in the main text and
the associated analysis.  (The full 3D data is too large to include here: please contact
the authors if you are interested in this.)

The main analysis is presented in the notebook
[`VortexLength.ipynb`](Figures/VortexLength.ipynb), which you can view using
[NBViewer](https://nbviewer.jupyter.org):

* [`VortexLength.ipynb` on NBViewer](https://nbviewer.jupyter.org/urls/gitlab.com/quantumturbulence/rotating_quantum_turbulence_ufg/-/raw/branch/default/Figures/VortexLength.ipynb)

The analysis can be run by clicking
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/quantumturbulence%2Frotating_quantum_turbulence_ufg/branch/default?filepath=Figures%2FVortexLength.ipynb).

*(Note: depending on how recently this was accessed, it may take some time for this to
load as MyBinder may need to first build the environment.)*

You can also cloning this repository, then install the necessary python tools using
[Conda](https://docs.conda.io/en/latest/):


```bash
conda env create -f environment.yml -n _rotating_qt_ufg
conda activate _rotating_qt_ufg
jupyter notebook Figures/VortexLength.ipynb
```

Note: to reproduce the plots, you will need to have a fairly complete installation of
[TeX
Live](https://www.google.com/search?client=safari&rls=en&q=texlive&ie=UTF-8&oe=UTF-8)
for generating labels with LaTeX.  This can be installed on Linux with the packages
listed in [`apt.txt`](apt.txt).  I.e.

```bash
apt-get install texlive-full
```



