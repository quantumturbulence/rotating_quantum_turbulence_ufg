"""Analysis tools for analyzing vortex length data."""
from collections import namedtuple

import numpy as np

from scipy.optimize import curve_fit

from uncertainties import correlated_values

from .data import VortexData

__all__ = ["VortexData", "FitDecay"]


class FitDecay:
    """Fit the decay model dL/dt = -alpha*(L-Linf)**(1+eps)."""

    # These are the physical parameters we want to use for the analysis, but they are
    # not the most convenient for actually fitting.
    Params = namedtuple("Params", ["eps", "Linf", "t0", "alpha"])

    # Here are the parameters in the form we will use to do the actual fit.
    FitParams = namedtuple("FitParams", ["eps", "Linf", "La", "alpha", "ta"])

    # Bounds for constraining the fit.
    bounds = FitParams(
        eps=[-0.2, 3.0],
        Linf=[1000, 1400],
        La=[0, np.inf],
        ta=[-np.inf, np.inf],
        alpha=[0, np.inf],
    )

    @classmethod
    def pf2p(cls, pf):
        """Return `Params` object from `FitParams` input."""
        eps, Linf, La, alpha, ta = pf
        t0 = ta - 1 / (alpha * eps * (La - Linf) ** eps)
        p = cls.Params(eps=eps, Linf=Linf, t0=t0, alpha=alpha)
        return p

    @classmethod
    def p2pf(cls, p, ta):
        """Return `FitParams` object from `Params` input."""
        eps, Linf, L0, alpha = p
        La = Linf + 1 / (ta - t0) ** (1 / eps) / alpha / eps
        pf = cls.FitParams(eps=eps, Linf=Linf, La=La, ta=ta, alpha=alpha)
        return pf

    @staticmethod
    def L(t, *p, d=0):
        """Model: single exponential from Params."""
        eps, Linf, t0, alpha = p
        L_Linf = 1 / (alpha * eps * (t - t0)) ** (1 / eps)
        if d == 0:
            return Linf + L_Linf
        elif d == 1:
            return -alpha * L_Linf ** (1 + eps)
        else:
            raise NotImplementedError

    @classmethod
    def Lf(cls, t, *pf, d=0):
        """Model: single exponential from FitParams."""
        return cls.L(t, *cls.pf2p(pf=pf), d=d)

    @classmethod
    def fit(cls, res, i, i1=None, fixed=None, p0=None, sigma=None, **kw):
        """Return the best fit parameters `p`.

        Arguments
        ---------
        i : int
           Offset into the data arrays.  Only data from [i_max+i:i1] are used
           where i_max = argmax(L_).
        fixed : dict
           Dictionary of fixed parameters.
        res : VortexLineData
           Data returned by load().
        p0 : Params
           Initial guess.

        Returns
        -------
        p : (ufloat,)
           Best fit parameters with correlated errors (0 for fixed parameters).
        """
        i0 = np.argmax(res.L_) + i
        inds = slice(i0, i1)
        ts = res.t_[inds]
        Ls = res.L_[inds]
        ta = ts[0]
        if p0 is None:
            eps = 1.0
            Linf = min(res.Linf_, 0.9 * res.L_[-1])
            dL_dt = np.gradient(Ls, ts)
            alpha = max(1e-5, (-dL_dt / (Ls - Linf) ** (1 + eps))[:20].mean())
            La = Ls[0]
            # t0 = (ts - 1 / (Ls - Linf)**eps / a / eps)[:20].mean()
            pf0 = cls.FitParams(eps=1.0, Linf=Linf, La=La, ta=ta, alpha=alpha)
            # print(pf0)
            # p0_ = get_guess(t=ts, L=Ls)
            # print(p0_)
        else:
            pf0 = cls.p2pf(cls.Params(*nominal_values(p0)))

        if fixed is None:
            fixed = {}
        fixed = dict(fixed, ta=ta)

        pf0_ = tuple(
            _p for _p, _k in zip(pf0, cls.FitParams._fields) if _k not in fixed
        )
        FitParams_ = namedtuple(
            "FitParams_", [_k for _k in cls.FitParams._fields if _k not in fixed]
        )
        pf0_ = FitParams_(*(getattr(pf0, _k) for _k in FitParams_._fields))

        def Lf_(t, *p0_):
            args = FitParams_(*p0_)._asdict()
            args.update(fixed)
            p0 = cls.pf2p(cls.FitParams(**args))
            return cls.L(t, *p0)

        args = dict(
            xdata=ts,
            ydata=Ls,
            p0=pf0_,
            bounds=[
                [getattr(cls.bounds, _k)[0] for _k in FitParams_._fields],
                [getattr(cls.bounds, _k)[1] for _k in FitParams_._fields],
            ],
            **kw
        )
        if sigma is None:
            args.update(absolute_sigma=False)
        else:
            if sigma is True:
                sigma = res.sigma_L_[inds]
            else:
                sigma = sigma[inds]

            args.update(absolute_sigma=True, sigma=sigma * np.ones_like(ts))
        popt, pcov = curve_fit(Lf_, **args)
        pf_ = FitParams_(
            *correlated_values(popt, covariance_mat=pcov, tags=FitParams_._fields)
        )
        pf = cls.FitParams(**dict(pf_._asdict(), **fixed))
        return cls.pf2p(pf), inds
